<?php
include_once("./models/db.php");
$sql="SELECT *FROM category";
$sql_prd="SELECT *FROM product INNER JOIN category ON product.cat_id=category.cat_id";
$sql_menu= 'SELECT * FROM menu INNER JOIN category ON menu.cat_id = category.cat_id';
$new=new db ();
$connect=$new->connectsql();
$query=$new->querysql($sql);
$query_prd=$new->querysql($sql_prd);
$query_menu=$new->querysql($sql_menu);
// $row=$new->fetch($sql);
// print_r($row);
if(isset($_GET['page_layout'])){
    switch ($_GET['page_layout']){
       case "add_cat":include_once("./views/add_cat.php");break;
       case "edit_cat":include_once("./views/edit_cat.php");break;
       case "edit_product":include_once("./views/edit_product.php");break;
       case "edit_menu":include_once("./views/edit_menu.php");break;
       case "add_product":include_once("./views/add_product.php");break;
       case "add_menu":include_once("./views/add_menu.php");break;
       case "product":include_once("./views/product.php");break;
       case "category":include_once("./views/category.php");break;
       case "menu":include_once("./views/menu.php");break;
       case "del_product":include_once("./views/del_product.php");break;
       case "del_cat":include_once("./views/del_cat.php");break;
    }
}else {
    include_once("./views/category.php");
}

?>