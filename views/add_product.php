   <!-- body -->
    <div id="add_product">
        <div class="container">
            <div class=" row place">
                <p>Home | Product | Thêm sản phẩm</p>
            </div>
            <div style="margin:40px 0 40px -15px;"><h3>Thêm sản phẩm</h3></div>
            <div class="row">
                <div class="form-add">
                    <form method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Tên sản phẩm</label>
                            <input name="prd_name" type="text" value="">
                        </div>
                        <div class="form-group">
                            <label>Danh mục</label>
                            <input name="cat_name" type="text" value="">
                        </div>
                        <div class="form-group">
                            <label>Giá sản phẩm</label>
                            <input name="prd_price" type="text" value="">
                        </div>
                        <div class="form-group">
                            <label>Dung lượng <br> (Khối lượng)</label>
                            <input name="prd_rom" type="text" value="">
                        </div>
                        <div class="form-group">
                            <label>Tình trạng</label>
                            <input name="prd_status" type="text" value="">
                        </div>
                       
                        <div class="form-image">
                            <label>Ảnh sản phẩm</label>
                            <input name="prd_image" type="file" value="">
                            <?php 
                              if(isset($_FILES['prd_image'])) {
                                $file_path=$_FILES['prd_image']['tmp_name'];
                                $prd_image=$_FILES ['prd_image']['name'];
                                move_uploaded_file($file_path,'./libs/img/'.$prd_image);
                              }                            
                            ?>
                        </div>
                        <div class="form-group">
                            <input name="sbm" type="submit" value="Thêm mới">
                            <!-- <button name="sbm" type="button" class="btn btn-success btn-sm">Thêm mới</button> -->
                        </div>
                        <?php
                          if(isset($_POST['sbm'])){
                            $prd_name=$_POST['prd_name'];
                            $cat_name=$_POST['cat_name'];
                            $prd_price=$_POST['prd_price'];
                            $prd_rom=$_POST['prd_rom'];
                            $prd_status=$_POST['prd_status'];
                            switch($cat_name){
                                case "Điện thoại-Máy tính bảng":$cat_id=10;break;
                                case "Mẹ và bé":$cat_id=11;break;
                                case "Laptop":$cat_id=12;break;
                                default :$cat_id=null;
                            }
                            $sql_2="INSERT INTO product(cat_id,prd_name,prd_price,prd_image,prd_rom,prd_status) VALUES('$cat_id','$prd_name','$prd_price','$prd_image','$prd_rom','$prd_status')";
                            $query=mysqli_query($connect,$sql_2);
                            header('location:index.php?page_layout=product');
                          } 
                        ?>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
