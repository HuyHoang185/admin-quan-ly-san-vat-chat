-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th7 14, 2021 lúc 09:50 PM
-- Phiên bản máy phục vụ: 10.4.19-MariaDB
-- Phiên bản PHP: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `san_vat_chat`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category`
--

CREATE TABLE `category` (
  `cat_id` int(11) NOT NULL,
  `cat_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `cat_parent` int(11) DEFAULT NULL,
  `id_level2` int(11) DEFAULT NULL,
  `cat_xem` int(11) DEFAULT NULL,
  `cat_mua` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `category`
--

INSERT INTO `category` (`cat_id`, `cat_name`, `cat_parent`, `id_level2`, `cat_xem`, `cat_mua`) VALUES
(2, 'Hàng mẹ và bé', 0, NULL, 0, 0),
(3, 'Thiết bị gia dụng', 0, NULL, 0, 0),
(4, 'Sức khỏe-Làm đẹp', 0, NULL, 0, 0),
(5, 'Hàng mẹ,bé', 0, NULL, 0, 0),
(6, 'Siêu thị-Tạp hóa', 0, NULL, 0, 0),
(7, 'Gia dụng', 0, NULL, 0, 0),
(8, 'Thời trang nữ', 0, NULL, 0, 0),
(9, 'Thời trang nam', 0, NULL, 0, 0),
(10, 'Điện thoại-Máy tính bảng', 1, 1, 0, 0),
(11, 'Mẹ và bé', 2, 1, 0, 0),
(12, 'Laptop', 1, 2, 0, 0),
(13, 'Apple', 10, NULL, 1, 1),
(14, 'Samsung', 10, NULL, 1, 0),
(15, 'Huawei', 10, NULL, 1, 0),
(16, 'Nokia', 10, NULL, 1, 1),
(17, 'Vsmart', 10, NULL, 1, 1),
(18, 'Oppo', 10, NULL, 1, 1),
(19, 'Vivo', 10, NULL, 1, 1),
(20, 'One plus', 10, NULL, 1, 0),
(21, 'Bphone', 10, NULL, 0, 1),
(22, 'Nokia', 10, NULL, 0, 1),
(23, 'Sony', 10, NULL, 1, 0),
(24, 'LG', 10, NULL, 0, 1),
(25, 'Áo bầu', 11, NULL, 0, 1),
(26, 'Bỉm ,tã', 11, NULL, 1, 0),
(27, 'Khăn ', 11, NULL, 1, 0),
(28, 'Váy ', 11, NULL, 0, 1),
(29, 'Sơ sinh', 11, NULL, 0, 1),
(30, 'Giày ,dép', 11, NULL, 0, 1),
(31, 'Sữa ', 11, NULL, 1, 1),
(32, 'Mũ ', 11, NULL, 1, 1),
(33, 'Macbook', 12, NULL, 1, 1),
(34, 'Dell', 12, NULL, 1, 0),
(35, 'Asus', 12, NULL, 0, 1),
(36, 'Acer', 12, NULL, 1, 0),
(37, 'HP', 12, NULL, 1, 1),
(38, 'Huawei', 12, NULL, 1, 0),
(39, 'Lenovo', 12, NULL, 1, 1),
(40, 'MSI', 12, NULL, 1, 0),
(41, 'Thiết bị điện tử ', 0, NULL, NULL, 0),
(42, 'Phụ kiện điện tử', 0, NULL, NULL, 0),
(43, 'Thời trang nam', 0, NULL, NULL, 0),
(45, 'Thời trang cho bé', 2, 2, NULL, 0),
(46, 'Phụ kiện cho bé', 2, 3, NULL, 0),
(47, 'Acer', 12, NULL, 1, 1),
(48, 'Asus', 12, NULL, 0, 1),
(49, 'Dell', 12, NULL, 0, 1),
(50, 'Apple', 12, NULL, 1, 0),
(51, 'Trang sức', 11, NULL, 1, 0),
(52, 'Kính', 11, NULL, 1, 0),
(53, 'Tã', 11, NULL, NULL, 0),
(54, 'Thời trang', 11, NULL, NULL, 0),
(55, 'Thiết bị IT', 1, 3, NULL, 0),
(56, 'Chuột máy tính', 55, NULL, NULL, 0),
(57, 'Bàn phím', 55, NULL, NULL, 0),
(58, 'Màn hình', 55, NULL, NULL, 0),
(59, 'USB', 55, NULL, NULL, 0),
(60, 'Ổ cứng SSD', 55, NULL, NULL, 0),
(61, 'Ổ cứng HHD', 55, NULL, NULL, 0),
(62, 'Đế tán nhiệt laptop', 55, NULL, NULL, 0),
(63, 'Card màn hình -VGA', 55, NULL, NULL, 0),
(64, 'Thiết bị khác', 55, NULL, NULL, 0),
(65, 'Khăn tắm', 45, NULL, NULL, 0),
(66, 'Áo choàng', 45, NULL, NULL, 0),
(67, 'Giày dép', 45, NULL, NULL, 0),
(68, 'Vali,balo', 45, NULL, NULL, 0),
(69, 'Dụng cụ tập ăn', 46, NULL, NULL, 0),
(70, 'Ghế ăn', 46, NULL, NULL, 0),
(71, 'Xe đẩy', 46, NULL, NULL, 0),
(72, 'Khăn giấy ướt', 46, NULL, NULL, 0),
(73, 'Thau bô,chậu tắm', 46, NULL, NULL, 0),
(74, 'Bình sữa ,bình tập uống', 46, NULL, NULL, 0),
(75, 'Điện tử -Điện lạnh', 1, 4, NULL, 0),
(76, 'Ti vi', 75, NULL, NULL, 0),
(77, 'Tủ lạnh', 75, NULL, NULL, 0),
(78, 'Máy giặt', 75, NULL, NULL, 0),
(79, 'Điều hòa', 75, NULL, NULL, 0),
(80, 'Máy in', 75, NULL, NULL, 0),
(81, 'Quạt hơi nước', 0, NULL, NULL, 0),
(82, 'Ổ điện', 75, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menu`
--

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL,
  `menu_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `cat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `menu`
--

INSERT INTO `menu` (`menu_id`, `menu_name`, `cat_id`) VALUES
(1, 'Iphone 8', 10),
(2, 'Iphone Xs', 10),
(3, 'Iphone 7', 10),
(4, 'Iphone 6s', 10),
(5, 'Grow Plus', 11),
(6, 'Nuti IQ', 11),
(7, 'Meta Care', 11),
(8, 'Nuti IQ', 11),
(9, 'Acer Swift 3', 12),
(10, 'Air M1', 12),
(11, 'Pro M1', 12),
(12, 'Acer Nitro', 12);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `prd_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `prd_name` varchar(255) NOT NULL,
  `prd_price` varchar(255) NOT NULL,
  `prd_image` varchar(255) NOT NULL,
  `prd_rom` varchar(11) NOT NULL,
  `prd_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `product`
--

INSERT INTO `product` (`prd_id`, `cat_id`, `prd_name`, `prd_price`, `prd_image`, `prd_rom`, `prd_status`) VALUES
(1, 10, 'Iphone Xs', '9.000.000', '1.png', '128GB', 4),
(2, 10, 'Iphone 7plus', '9.000.000', '2.png', '64GB', 6),
(3, 10, 'Samsung Galaxy S8', '25.000.000', '3.png', '64GB', 0),
(4, 10, 'Nokia 8.1', '1.100.000', '4.png', '128GB', 1),
(5, 10, 'Huawei Nova 2i', '8.760.000', '5.png', '16GB', 0),
(6, 10, 'Nokia 8.3', '1.020.000', '6.png', '8GB', 3),
(7, 10, 'Samsung J7 pro', '9.400.000', '7.png', '32GB', 9),
(8, 11, 'Grow Plus', '1.500.000', '8.jpg', '32g', 3),
(9, 11, 'Nuti IQ', '1.200.000', '9.jpg', '16g', 6),
(10, 11, 'Coloscare', '12.000.000', '10.jpg', '128g', 9),
(11, 11, 'Grow Plus', '25.000.000', '11.jpg', '256g', 5),
(12, 11, 'Nutifood', '2.500.000', '12.jpg', '128g', 8),
(13, 11, 'Nuti IQ', '3.800.000', '13.jpg', '64g', 0),
(14, 11, 'Meta Care', '7.600.000', '14.jpg', '16g', 0),
(15, 12, 'Acer Nitro 5', '9.400.000', '15.jpg', '256GB', 1),
(16, 12, 'Acer Nitro 7', '4.500.000', '16.jpg', '512GB', 0),
(17, 12, 'Acer Swift ', '6.700.000', '17.jpg', '16GB', 3),
(18, 12, 'Acer Aspire', '13.000.000', '18.jpg', '64GB', 7),
(19, 12, 'Macbook Air M1', '9.000.000', '19.jpg', '32GB', 3),
(20, 12, 'Macbook Pro M1', '12.000.000', '20.jpg', '64GB', 4),
(21, 12, 'Vivobook', '3.000.000', '21.jpg', '128GB', 8),
(23, 10, 'Oneplus 1', '1.000.000', 'Xiaomi-Redmi-Note-6-Pro–32GB-Blue.png', '128GB', 0);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Chỉ mục cho bảng `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`prd_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `category`
--
ALTER TABLE `category`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT cho bảng `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `product`
--
ALTER TABLE `product`
  MODIFY `prd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
